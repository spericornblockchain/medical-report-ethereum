export interface ReportModel {
  reportId: number
  hospitalId: number
  doctorId: number
  reportedTime: number
  prescription: string
  allowedTime: number
  status: boolean
}
